module keys; 
import std.uni : asUpperCase;
import std.string : isNumeric;
import std.conv : to;
import std.exception;

/// Convert string to key id
uint toKeyId(string keyName){
	if (keyName.isNumeric) // key by id
		return keyName.to!uint;
	if (keyName.length == 4 && keyName[0 .. 2].asUpperCase.to!string == "VK.") // key not from enum
		if ((keyName[3] >= 'A' && keyName[3] <= 'Z') || (keyName[3] >= '0' && keyName[3] <= '9'))
			return cast(uint)keyName[3];
	try{
		return cast(uint)keyName[3..$].to!VK;
	} catch (Exception){}
	return 0xFFFFFFFF; // no match key
}

/// Convert key id to string
string toKeyName(uint keyId){
	if ((keyId >= 'A' && keyId <= 'Z') || (keyId >= '0' && keyId <= '9')) // key not from enum
		return "VK."~cast(char)keyId;
	try{
		return "VK."~(cast(VK)keyId).to!string;
	} catch (Exception){}
	return keyId.to!string; // key by id
}

/// Virtual keys
enum VK {
	LBUTTON = 0x01,
	RBUTTON,
	MBUTTON = 0x04,
	XBUTTON1,
	XBUTTON2,
	BACK = 0x08,
	TAB,
	CAPITAL = 0x14,
	RETURN = 0x0D,
	SHIFT = 0x10,
	LSHIFT = 0xA0,
	RSHIFT,
	CONTROL = 0x11,
	LCONTROL = 0xA2,
	RCONTROL,
	MENU = 0x12,
	LMENU = 0xA4,
	RMENU,
	SPACE = 0x20,
	PRIOR,
	NEXT,
	END,
	HOME,
	LEFT,
	UP,
	RIGHT,
	DOWN,
	INSERT = 0x2D,
	DELETE,
	PAUSE = 0x13,
	NP_0 = 0x60,
	NP_1,
	NP_2,
	NP_3,
	NP_4,
	NP_5,
	NP_6,
	NP_7,
	NP_8,
	NP_9,
	NP_MULTIPLY,
	NP_PLUS,
	NP_SEPARATOR,
	NP_MINUS,
	NP_DECIMAL,
	NP_DIVIDE,
	F1,
	F2,
	F3,
	F4,
	F5,
	F6,
	F7,
	F8,
	F9,
	F10,
	F11,
	F12,
	F13,
	F14,
	F15,
	F16,
	F17,
	F18,
	F19,
	F20,
	F21,
	F22,
	F23,
	F24,
	OEM_PLUS = 0xBB,
	OEM_COMMA,
	OEM_MINUS,
	OEM_PERIOD,
	OEM_1 = 0xBA,
	OEM_2 = 0xBF,
	OEM_3,
	OEM_4 = 0xDB,
	OEM_5,
	OEM_6,
	OEM_7,
	OEM_8,
	BROWSER_BACK = 0xA6,
	BROWSER_FORWARD,
	BROWSER_REFRESH,
	BROWSER_STOP,
	BROWSER_SEARCH,
	BROWSER_FAVORITES,
	BROWSER_HOME,
	VOLUME_MUTE,
	VOLUME_DOWN,
	VOLUME_UP,
	MEDIA_NEXT_TRACK,
	MEDIA_PREV_TRACK,
	MEDIA_STOP,
	MEDIA_PLAY_PAUSE,
	LAUNCH_MAIL,
	LAUNCH_MEDIA_SELECT,
	LAUNCH_APP1,
	LAUNCH_APP2
}

unittest{
	assert("VK.TAB".toKeyId == 0x09);
	assert("9".toKeyId == 0x09);
	assert("VK.a".toKeyId == 0x41);
	assert(toKeyName(0x09) == "VK.TAB");
}