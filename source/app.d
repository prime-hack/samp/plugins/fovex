import core.sys.windows.windows, core.sys.windows.dll, core.runtime;
import inifiled, keys;
import std.file : exists, isFile;
import std.concurrency : spawn, yield;
import std.math : pow;

/// Ini file name for save and load settings
enum iniFile = "plugins/FovEx.ini";
__gshared stConfig conf; /// config file

/// Initialize this plugin
void initialize(){
	// Patch sniper zooming
	mem!(float*).write(0x5109A3, &_maxZoom);
	mem!(float*).write(0x5109BC, &_maxZoom);

	// Load ini
	if (iniFile.exists && iniFile.isFile)
		conf.readINIFile(iniFile);
	prepareFov(zoomFactor());
	conf.inc = toKeyId(conf._inc);
	conf.dec = toKeyId(conf._dec);

	// Hook WndProc
	hOriginalProc = cast(WNDPROC)SetWindowLongA( *cast(HWND*)0xC97C1C, GWL_WNDPROC, cast(LONG)&WndProc );
}

/// Deinitialize this plugin
void deinitialize(){
	// Restore WndProc
	SetWindowLongA( *cast(HWND*)0xC97C1C, GWL_WNDPROC, cast(LONG)hOriginalProc );

	// Save ini
	conf.writeINIFile(iniFile);

	// Restore sniper zooming
	mem!(uint).write(0x5109A3, 0x1D12000);
	mem!(uint).write(0x5109BC, 0x1D12000);

	// Restore fovs
	mem!(float).write(0x8CC4B4, 50.0f); // group rifle1 (m4 and ak47)
	mem!(float).write(0x8CC4B4, 35.0f); // group rifle2 (rifle)
	mem!(float).write(0x5109B0, 15.0f); // Sniper rifle (const)
	mem!(float).write(0x5109cd, 15.0f); // Sniper rifle (set max)
	mem!(float).write(0x521632, 70.0f); // Other weapons

	// Restore zoom speed
	mem!(float).write(0x858FC4, 0.0001f); // Sniper rifle
	mem!(float).write(0x862F1C, 1.0f); // Other weapons

	// Restore sens
	mem!(float).write(0x8CC4A0, 0.007f); // Sticks
	mem!(float).write(0x8CC4A8, 0.1f); // Free target
}

/// Replace game variable maxZoom for 0x5109A3 and 0x5109BD (sniper rifle)
__gshared static float _maxZoom = 15.0f; // @suppress(dscanner.unnecessary.duplicate_attribute)

/// Change FOV in aiming mode
void prepareFov(float fovmul) {
	// Fovs
	mem!(float).write(0x8CC4B4, 50.0f * (1.0f / fovmul)); // group rifle1 (m4 and ak47)
	mem!(float).write(0x8CC4B4, 35.0f * (1.0f / fovmul)); // group rifle2 (rifle)
	_maxZoom = 15.0f * (1.0f / fovmul); // Sniper rifle (compare)
	mem!(float).write(0x5109B0, 15.0f * (1.0f / fovmul)); // Sniper rifle (const)
	mem!(float).write(0x5109cd, 15.0f * (1.0f / fovmul)); // Sniper rifle (set max)
	mem!(float).write(0x521632, 70.0f * (1.0f / fovmul)); // Other weapons

	// Zoom speed
	mem!(float).write(0x858FC4, 0.0001f * fovmul); // Sniper rifle
	mem!(float).write(0x862F1C, fovmul); // Other weapons

	// Sens
	mem!(float).write(0x8CC4A0, 0.007f * (1.0f / fovmul)); // Sticks
	mem!(float).write(0x8CC4A8, 0.1f * (1.0f / fovmul)); // Free target
}

/// Calc multiple value for zoom
float zoomFactor(){
	if (conf.expZoom)
		return pow(2.718, conf.zoomStep * conf.zoomLevel);
	else return 1.0f + conf.zoomStep * conf.zoomLevel;
}

/// entrypoint of this library
extern (Windows) BOOL DllMain(HINSTANCE hInstance, ULONG ulReason, LPVOID) // @suppress(dscanner.style.phobos_naming_convention)
{
	switch (ulReason)
	{
		case DLL_PROCESS_ATTACH:
			Runtime.initialize();
			spawn({
				while (*cast(uint*)0xC8D4C0 < 7)
					yield();
				initialize();
			});
			dll_process_attach(hInstance, true);
			break;
		case DLL_PROCESS_DETACH:
			deinitialize();
			Runtime.terminate();
			dll_process_detach(hInstance, true);
			break;
		case DLL_THREAD_ATTACH:
			dll_thread_attach(true, true);
			break;
		case DLL_THREAD_DETACH:
			dll_thread_detach(true, true);
			break;
		
		default:
			break;
	}
	return true;
}

__gshared WNDPROC hOriginalProc; /// Original WndProc of current proccess
/// hook WndProc for key events
extern (Windows) LRESULT WndProc( HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam ){ // @suppress(dscanner.style.phobos_naming_convention)
	if (hwnd == *cast(HWND*)0xC97C1C){
		if ( uMsg == WM_XBUTTONDOWN && ( wParam == 0x10020 || wParam == 0x20040 ) ) {
			if ( wParam == 0x10020 )
				wParam = VK_XBUTTON1;
			else if ( wParam == 0x20040 )
				wParam = VK_XBUTTON2;
		} else if ( uMsg == WM_XBUTTONUP && ( wParam == 0x10020 || wParam == 0x20040 ) ) {
			if ( wParam == 0x10020 )
				wParam = VK_XBUTTON1;
			else if ( wParam == 0x20040 )
				wParam = VK_XBUTTON2;
		}
		if ( wParam == VK_SHIFT || wParam == VK_CONTROL || wParam == VK_MENU ) {
			immutable(int) scancode = ( lParam >> 16 ) & 0x00FF;
			if ( scancode == MapVirtualKey( VK_LSHIFT, 0 ) )
				wParam = VK_LSHIFT;
			else if ( scancode == MapVirtualKey( VK_RSHIFT, 0 ) )
				wParam = VK_RSHIFT;
			else if ( scancode == MapVirtualKey( VK_LCONTROL, 0 ) )
				wParam = VK_LCONTROL;
			else if ( scancode == MapVirtualKey( VK_RCONTROL, 0 ) )
				wParam = VK_RCONTROL;
			else if ( scancode == MapVirtualKey( VK_LMENU, 0 ) )
				wParam = VK_LMENU;
			else if ( scancode == MapVirtualKey( VK_RMENU, 0 ) )
				wParam = VK_RMENU;
		}

		__gshared static bool rmb_down; // @suppress(dscanner.unnecessary.duplicate_attribute)
		if (uMsg == WM_RBUTTONDOWN)
			rmb_down = true;
		else if (uMsg == WM_RBUTTONUP)
			rmb_down = false;
		
		if (rmb_down && (uMsg == WM_KEYDOWN || uMsg == WM_SYSKEYDOWN)){
			if (conf.switchOnNum && wParam >= '1' && wParam <= '9')
				conf.zoomLevel = wParam - '1';
			if (wParam == conf.inc)
				conf.zoomLevel++;
			if (wParam == conf.dec && conf.zoomLevel > 0)
				conf.zoomLevel--;
			prepareFov(zoomFactor());
		}
	}
	return CallWindowProcA(hOriginalProc, hwnd, uMsg, wParam, lParam);
}

/++
 + Memory operations with type
 +/
template mem(T){
	void write(uint addr, T value, SafeLevel safeLevel = SafeLevel.vp) @nogc {
		uint vp;
		if (safeLevel == SafeLevel.vp)
			vp = protect(addr, 0x40);
		*cast(T*)addr = value; // set value
		if (safeLevel == SafeLevel.vp)
			protect(addr, vp);
	}
	T read(uint addr, SafeLevel safeLevel = SafeLevel.vp) @nogc {
		uint vp;
		if (safeLevel == SafeLevel.vp)
			vp = protect(addr, 0x40);
		T result = *cast(T*)addr; // get value
		if (safeLevel == SafeLevel.vp)
			protect(addr, vp);
		return result;
	}
	/++
	 + Get/set protection on memory
	 + pass 'prot' is 0 to get current protection
	 +/
	uint protect(uint addr, uint prot = 0) @nogc {
		if (prot){
			uint vp;
			VirtualProtect(cast(void*)addr, T.sizeof, prot, &vp);
			return vp;
		}
		MEMORY_BASIC_INFORMATION mbi;
		VirtualQuery(cast(void*)addr, &mbi, T.sizeof);
		return mbi.Protect;
	}
}
/// Level to safe memory operations
enum SafeLevel : byte {
	unsafe = 0,
	vp = 1,
	safe = 2 // unused
}

/// Struct with plugin settings
@INI("FovEx settings", "Config") struct stConfig{ // @suppress(dscanner.style.phobos_naming_convention)
	@INI("Zoom level", "zoom") uint zoomLevel = 0; // @suppress(dscanner.style.undocumented_declaration)
	@INI("Zoom step", "step") float zoomStep = 0.1f; // @suppress(dscanner.style.undocumented_declaration)
	@INI("Exponential zooming. If disabled uses linear zooming", "expanent") bool expZoom = false; // @suppress(dscanner.style.undocumented_declaration)
	@INI("Use numbers for fast switch zoom factor", "numbers switcher") bool switchOnNum = false; // @suppress(dscanner.style.undocumented_declaration)
	@INI("Key for increase zoom", "increase key") string _inc = "VK.NP_PLUS"; // @suppress(dscanner.style.undocumented_declaration)
	@INI("Key for decrease zoom", "decrease key") string _dec = "VK.NP_MINUS"; // @suppress(dscanner.style.undocumented_declaration)

	uint inc; /// key id for increase zoom
	uint dec; /// key id for decrease zoom
}